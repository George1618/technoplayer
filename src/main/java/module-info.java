module technoplayer {
    requires javafx.controls;
    requires javafx.fxml;
    requires mp3agic;
    
    opens br.ufc.tecprog.technoplayer to javafx.fxml;
    exports br.ufc.tecprog.technoplayer;
}
