package br.ufc.tecprog.technoplayer.models;

import java.util.UUID;

public class Music {

    final UUID id;
    String title;
    Artist artist;
    Integer year;
    short genre;

    public Music() {
        this.id = UUID.randomUUID();
    }

    public UUID getID() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public short getGenre() {
        return genre;
    }

    public void setGenre(short genre) {
        this.genre = genre;
    }
}
