package br.ufc.tecprog.technoplayer.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Artist {

    final UUID id;
    String name;
    List<UUID> ArtistMusicsIDs = new ArrayList<>();

    public Artist(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getArtistMusicsIDs() {
        return ArtistMusicsIDs;
    }

    public void addMusicID(Music music) {
        ArtistMusicsIDs.add(music.getID());
    }

    public void removeMusicID(Music music) {
        ArtistMusicsIDs.remove(music.getID());
    }


}
