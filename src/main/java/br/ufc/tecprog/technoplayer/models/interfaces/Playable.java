package br.ufc.tecprog.technoplayer.models.interfaces;

public interface Playable {
    void previous();
    void play();
    void next();
}
