package br.ufc.tecprog.technoplayer.models.queues;

import java.util.List;

public abstract class Queue<T> {
    Integer index;
    List<T> elems;
}
