package br.ufc.tecprog.technoplayer.models.playlists;

import br.ufc.tecprog.technoplayer.models.Music;

import java.util.List;
import java.util.UUID;

public abstract class Playlist {

    final UUID id;
    final String creator;
    List<Music> musics;
    String name;


    Playlist(String creator) {
        this.id = UUID.randomUUID();
        this.creator = creator;
    }

}
