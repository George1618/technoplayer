package br.ufc.tecprog.technoplayer.models.playlists;

import br.ufc.tecprog.technoplayer.models.Artist;

public class ArtistPlaylist extends Playlist {

    Artist artist;

    public ArtistPlaylist(String creator, Artist artist) {
        super(creator);
        this.artist = artist;
    }


}
