package br.ufc.tecprog.technoplayer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class TechnoPlayerApplication extends Application {

    static FXMLLoader loader;
    static Scene scene;

    @Override
    public void init() throws Exception {
        super.init();
        this.loader = new FXMLLoader();
        this.scene = new Scene(new AnchorPane());
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }


    public static void main(String[] args) {
        launch(TechnoPlayerApplication.class, args);
    }

}
