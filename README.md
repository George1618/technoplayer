# TechnoPlayer 🎼

TechnoPlayer - Projeto final da cadeira CK0235 - TÉCNICAS DE PROGRAMAÇÃO I (2020.1)

### Descrição do projeto 🧾

- UML: [LucidChart](https://app.lucidchart.com/invitations/accept/3a053bc4-695b-445d-bd5c-eb5699e52d16)
- Mockup: [Figma](https://www.figma.com/file/3XuGWz58YOMX038z1uBkTG/MusicPlayer-TecProg1)